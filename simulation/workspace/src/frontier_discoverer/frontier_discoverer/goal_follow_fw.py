import imp
import sys
import time

from action_msgs.msg import GoalStatus
from geometry_msgs.msg import PoseStamped, PoseWithCovarianceStamped
from nav2_msgs.action import FollowWaypoints

import rclpy
from rclpy.action import ActionClient
from rclpy.node import Node


class GoalFollower(Node):

    def __init__(self):
        super().__init__(node_name='nav2_follow_goal')
        self._client = ActionClient(self, FollowWaypoints, '/FollowWaypoints')

    
    def set_initial_pose(self, pose):

        self.init_pose = PoseWithCovarianceStamped()
        self.init_pose.pose.pose.position.x = pose[0]
        self.init_pose.pose.pose.position.y = pose[1]
        self.init_pose.header.frame_id = 'map'
        self.currentPose = self.init_pose.pose.pose
        self.publishInitialPose()
        time.sleep(5)

    
    def publishInitialPose(self):

        self.initial_pose_pub.publish(self.init_pose)

    
    def follow(self, goal):

        # waypoints = []
        # msg = PoseStamped()
        # msg.header.frame_id = '/map'
        # msg.header.stamp = self.get_clock().now()
        # msg.pose.position.x = goal[0]
        # msg.pose.position.y = goal[1]
        # msg.pose.orientation.w = 1.0
        # waypoints.append(msg)

        # TODO:
        #       1. FollowWaypoints.Goal()
        #       2. msg
        #       3. ActionClient...

        msg = FollowWaypoints.Goal()
        msg.poses = goal

        self.info_msg("Waiting for the server...")
        self._client.wait_for_server()
        self.info_msg("Sending request...")
        self._send_goal_future = self._client.send_goal_async(msg, feedback_callback=self.feedback_callback)
        self.info_msg("Adding done...")
        self._send_goal_future.add_done_callback(self.goal_response_callback)
        
    
    def goal_response_callback(self, future):

        goal_handle = future.result()
        
        if not goal_handle.accepted:
            self.info_msg('Goal rejected')
            return

        self.info_msg('Goal accepted')

        self._get_result_future = goal_handle.get_result_async()
        self._get_result_future.add_done_callback(self.get_result_callback)

    
    def get_result_callback(self, future):

        result = future.result().result
        self.info_msg('Result: {0}'.format(result.missed_waypoints))

    
    def feedback_callback(self, feedback_msg):

        feedback = feedback_msg.feedback
        self.info_msg('Received feedback: {0}}'.format(feedback.current_waypoint))


    def info_msg(self, msg: str):

        self.get_logger().info(msg)


    def error_msg(self, msg: str):

        self.get_logger().error(msg)


def main():

    rclpy.init()
    starting_pose = [21.32115109682464, -23.17114582423655]
    final_pose = [22.5, -23.0]

    goal_follower = GoalFollower()

    rgoal = PoseStamped()
    rgoal.header.frame_id = "map"
    rgoal.header.stamp.sec = 0
    rgoal.header.stamp.nanosec = 0
    rgoal.pose.position.z = 0.0
    rgoal.pose.position.x = 22.5
    rgoal.pose.position.y = -23.0
    rgoal.pose.orientation.w = 1.0
    print(rgoal)
    mgoal = [rgoal]

    goal_follower.follow(mgoal)

    rclpy.spin(goal_follower)



if __name__ == '__main__':
    main()