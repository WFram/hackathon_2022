from cmath import sqrt
from distutils.log import info
import imp
from importlib import import_module
from lib2to3.pytree import Node

import sys
import time
import numpy as np
import math
# from scipy.spatial.distance import cdist, euclidean

from enum import Enum

from action_msgs.msg import GoalStatus
from geometry_msgs.msg import PoseStamped, PoseWithCovarianceStamped, Twist
from nav2_msgs.action import FollowWaypoints
from nav2_msgs.srv import ManageLifecycleNodes
from nav2_msgs.srv import GetCostmap
from nav2_msgs.msg import Costmap
from nav_msgs.msg  import OccupancyGrid
from nav_msgs.msg import Odometry

from geometry_msgs.msg import PoseStamped

import rclpy
from rclpy.duration import Duration
from rclpy.action import ActionClient
from rclpy.node import Node
from rclpy.qos import QoSDurabilityPolicy, QoSHistoryPolicy, QoSReliabilityPolicy
from rclpy.qos import QoSProfile

from nav2_simple_commander.robot_navigator import BasicNavigator, TaskResult
# from nav2_behaviors import 


OCC_THRESHOLD = 10
MIN_FRONTIER_SIZE = 5

class Costmap2d():
    class CostValues(Enum):
        FreeSpace = 0
        InscribedInflated = 253
        LethalObstacle = 254
        NoInformation = 255
    
    def __init__(self, map):
        self.map = map

    def getCost(self, mx, my):
        return self.map.data[self.__getIndex(mx, my)]

    def getSize(self):
        return (self.map.metadata.size_x, self.map.metadata.size_y)

    def getSizeX(self):
        return self.map.metadata.size_x

    def getSizeY(self):
        return self.map.metadata.size_y

    def __getIndex(self, mx, my):
        return my * self.map.metadata.size_x + mx


class OccupancyGrid2d():
    class CostValues(Enum):
        FreeSpace = 0
        InscribedInflated = 100
        LethalObstacle = 100
        NoInformation = -1

    # When the object created, set the map attribute. The attribute is a map msg
    def __init__(self, map):
        self.map = map

    # Cost means occupancy probability (http://docs.ros.org/en/noetic/api/nav_msgs/html/msg/OccupancyGrid.html)
    def getCost(self, mx, my):
        return self.map.data[self.__getIndex(mx, my)]

    def getSize(self):
        return (self.map.info.width, self.map.info.height)

    def getSizeX(self):
        return self.map.info.width

    def getSizeY(self):
        return self.map.info.height

    def mapToWorld(self, mx, my):
        wx = self.map.info.origin.position.x + (mx + 0.5) * self.map.info.resolution
        wy = self.map.info.origin.position.y + (my + 0.5) * self.map.info.resolution

        return (wx, wy)

    # Convert odom pose in [m] to odom pose in [cells] (map frame)
    def worldToMap(self, wx, wy):
        # Compare to the real-world pose of the cell (0,0) in the map
        if (wx < self.map.info.origin.position.x or wy < self.map.info.origin.position.y):
            print('wx: ', wx)
            print('wy: ', wy)
            print('self.map.info.origin.position.x: ', self.map.info.origin.position.x)
            print('self.map.info.origin.position.y', self.map.info.origin.position.y)
            raise Exception("World coordinates out of bounds")

        # Get current pose in map frame, but measured in map cells
        mx = int((wx - self.map.info.origin.position.x) / self.map.info.resolution)
        my = int((wy - self.map.info.origin.position.y) / self.map.info.resolution)
        
        if  (my > self.map.info.height or mx > self.map.info.width):
            raise Exception("Out of bounds")

        return (mx, my)

    # http://docs.ros.org/en/noetic/api/nav_msgs/html/msg/OccupancyGrid.html
    # By the index we can identify, which probability is in the cell
    def __getIndex(self, mx, my):
        # y * width + x, where width is Map Width in cells (see format)
        # TODO: why?
        return my * self.map.info.width + mx

class FrontierCache():
    # create a dictionary
    cache = {}

    # Give a cell coordinates wrapped in FrontierPoint
    def getPoint(self, x, y):
        # get a unique index for a point
        idx = self.__cantorHash(x, y)

        # if a point is already exists in cash, then return corresponding FrontierPoint object
        if idx in self.cache:
            return self.cache[idx]
        # otherwise, create a new object
        self.cache[idx] = FrontierPoint(x, y)
        return self.cache[idx]


    def __cantorHash(self, x, y):
        return (((x + y) * (x + y + 1)) / 2) + y

    def clear(self):
        self.cache = {}

# Stores: 1) point coordinates, 2) point class
class FrontierPoint():
    def __init__(self, x, y):
        self.classification = 0
        self.mapX = x
        self.mapY = y

def centroid(arr):

    arr = np.array(arr)
    length = arr.shape[0]
    # TODO: restrict a number of frontiers to consider
    sum_x = np.sum(arr[:10, 0])
    sum_y = np.sum(arr[:10, 1])
    return sum_x / 10, sum_y / 10


def geometric_median(X, numIter = 200):
    # -- Initialising 'median' to the centroid
    X = np.array(X)
    y = np.mean(X, 0)
    # -- If the init point is in the set of points, we shift it:
    # while (y[:, 0] in X[:, 0]) and (y[:, 1] in X[:, 1]):
    #     y += 0.1
    print(f'y shape: {y}')
    print(f'X shape: {X}')

    convergence = False # boolean testing the convergence toward a global optimum
    dist = [] # list recording the distance evolution

    # -- Minimizing the sum of the squares of the distances between each points in 'X' and the median.
    i = 0
    while ((not convergence) and (i < numIter)):
        
        num_x, num_y = 0.0, 0.0
        denum = 0.0
        m = X.shape[0]
        d = 0
        for j in range(0, m):
            div = math.sqrt((X[j, 0] - y[0])**2 + (X[j, 1] - y[1])**2)
            num_x += X[j, 0] / div
            num_y += X[j, 1] / div
            denum += 1. / div
            d += div**2 # distance (to the median) to miminize
        dist.append(d) # update of the distance evolution

        if denum == 0.:
            print("Couldn't compute a geometric median, please check the data!")
            return [0, 0, 0]

        y = [num_x / denum, num_y / denum] # update to the new value of the median
        if i > 3:
            convergence = (abs(dist[i] - dist[i - 2]) < 0.1) # we test the convergence over three steps for stability
            #~ print abs(dist[i]-dist[i-2]), convergence
        i += 1
    if i == numIter:
        raise ValueError("The Weiszfeld's algoritm did not converged after" + str(numIter) + "iterations")
    # -- When convergence or iterations limit is reached we assume that we found the median.

    return y[0], y[1]


def dist_sum(arr, x_c, y_c):

    # arr = np.array(arr)
    n = arr.shape[0]
    dist_sum = 0
    for i in range(0, n):
        dist_x = abs(arr[i, 0] - x_c)
        dist_y = abs(arr[i, 1] - y_c)
        dist_sum += sqrt((dist_x * dist_x) + (dist_y * dist_y)).real
    print(f'dist_x: {dist_x}')
    print(f'dist_y: {dist_y}')
    print(f'dist_sum: {dist_sum}')

    return dist_sum


def median(arr):

    arr = np.array(arr)
    n = arr.shape[0]
    x_c = 0.0
    y_c = 0.0
    for i in range(0, n):
        x_c += arr[i, 0]
        y_c += arr[i, 1]

    x_c /= n
    y_c /= n

    min_dist = dist_sum(arr, x_c, y_c)
    print(f'Min dist: {min_dist}')

    k = 0
    while k < n:
        for i in range(0, n):
            if i != k:
                x_n = 0.0
                y_n = 0.0
                x_n = arr[i, 0]
                y_c = arr[i, 1]
                newd = dist_sum(arr, x_n, y_n)
                if newd < min_dist:
                    min_dist = newd
                    x_c = x_n
                    y_c = y_n
        k += 1

    # test_distance = 1000
    # flag = 0

    return x_c, y_c


def findFree(mx, my, costmap):
    fCache = FrontierCache()

    # Create object of class FrontierPoint from given point
    # array is useless here; always one point can be
    bfs = [fCache.getPoint(mx, my)]

    # Always being run
    # Morever! If the given cell is NOT free, then it will search until finding a free cell among neighbours
    # If no free cells, it will return a current cell coordinates
    # Hence not good. It will returns an initial cell, even though it might be occupied
    while len(bfs) > 0:
        # take a point from above and clear the bfs (the next time this block will not be completed)
        loc = bfs.pop(0)

        # If the cell is free, then give it
        if costmap.getCost(loc.mapX, loc.mapY) == OccupancyGrid2d.CostValues.FreeSpace.value:
            return (loc.mapX, loc.mapY)

        # If the cell is NOT free, then ...
        for n in getNeighbors(loc, costmap, fCache):
            # First cell classification. For each classify as MapClosed
            # Anyways, it find a neighbour ONLY if a cell is NOT classified
            if n.classification & PointClassification.MapClosed.value == 0:
                n.classification = n.classification | PointClassification.MapClosed.value
                bfs.append(n)

    return (mx, my)

def getFrontier(pose, costmap, logger):
    fCache = FrontierCache()

    fCache.clear()

    # Get a current pose (robot pose) in map cells (map frame)
    mx, my = costmap.worldToMap(pose.position.x, pose.position.y)
    # print(f'X, Y: {[pose.position.x, pose.position.y]}')
    # print(f'X, Y: {[mx, my]}')
    # mx = pose.position.x
    # my = pose.position.y

    # An array of free cell coordinates
    freePoint = findFree(mx, my, costmap)
    start = fCache.getPoint(freePoint[0], freePoint[1])

    # First, the cell is classified as MapOpen
    start.classification = PointClassification.MapOpen.value
    
    mapPointQueue = [start]

    frontiers = []

    # TODO: choose the best one
    while len(mapPointQueue) > 0:
        p = mapPointQueue.pop(0)

        # Don't consider a cell, if it is classified as MapOpen
        if p.classification & PointClassification.MapClosed.value != 0:
            continue

        # Two conditions: 1) the cell is unknown; 2) at least one neighbour is free; 3) no occupied surrounded
        if isFrontierPoint(p, costmap, fCache):
            p.classification = p.classification | PointClassification.FrontierOpen.value
            # Contains frontiers
            frontierQueue = [p]
            newFrontier = []

            # First, it's completed always
            while len(frontierQueue) > 0: # TODO: no need to check
                q = frontierQueue.pop(0)

                # Doesn't consider a cell, if:
                # 1. MapClosed
                # 2. FrontierClosed
                if q.classification & (PointClassification.MapClosed.value | PointClassification.FrontierClosed.value) != 0:
                # if q.classification & PointClassification.FrontierClosed.value != 0:    
                    continue

                if isFrontierPoint(q, costmap, fCache):
                    newFrontier.append(q)

                    # Search frontiers among neighbours
                    for w in getNeighbors(q, costmap, fCache):
                        # Only consider those which are either not classified, or MapOpen
                        if w.classification & (PointClassification.FrontierOpen.value | PointClassification.FrontierClosed.value | PointClassification.MapClosed.value) == 0:
                            # Classify as FrontierOpen
                            w.classification = w.classification | PointClassification.FrontierOpen.value
                            frontierQueue.append(w)
                    # Thus, the amount of frontiers can be high

                q.classification = q.classification | PointClassification.FrontierClosed.value

            # if len(frontierQueue) > 0:

            # Fill with frontier coords
            newFrontierCords = []
            for x in newFrontier:
                x.classification = x.classification | PointClassification.MapClosed.value
                newFrontierCords.append(costmap.mapToWorld(x.mapX, x.mapY))

            print('Amount of new frontiers {0}'.format(str(len(newFrontier))))
            if len(newFrontier) > MIN_FRONTIER_SIZE:
                print(f'New frontier coordinates: {newFrontierCords}')
                # TODO: Choose the closest one
                frontiers.append(centroid(newFrontierCords))
                print('New median coordinates {0}'.format(frontiers[-1]))
                # break

            # Basically, by this moment, we just took the current pose and had a look at cells
            # in a very small window
            # It seems we need either expand this window smartly, or 

            # if len(frontiers) < 3:
                

        ################################################################
        ################################################################
        ################################################################
        # Done with the current pose. Now search frontiers for neighbours of current pose
        for v in getNeighbors(p, costmap, fCache):
            # Consider a cell only if it's a frontier
            if v.classification & (PointClassification.MapOpen.value | PointClassification.MapClosed.value) == 0:
                if any(costmap.getCost(x.mapX, x.mapY) == OccupancyGrid2d.CostValues.FreeSpace.value for x in getNeighbors(v, costmap, fCache)):
                    v.classification = v.classification | PointClassification.MapOpen.value
                    mapPointQueue.append(v)

        p.classification = p.classification | PointClassification.MapClosed.value

    # mapPointQueue.clear()

    return frontiers
        

def getNeighbors(point, costmap, fCache):
    
    neighbors = []

    window_size = 20
    # Search in rectangle
    # for x in range(point.mapX - 1, point.mapX + 2):
        # for y in range(point.mapY - 1, point.mapY + 2):
    for x in range(point.mapX - window_size, point.mapX + window_size):
        for y in range(point.mapY - window_size, point.mapY + window_size):
            # If the cell is NOT out of bounds
            if (x > 0 and x < costmap.getSizeX() and y > 0 and y < costmap.getSizeY()):
            # if (x > 0 and x < costmap.getSizeX() and y > 0 and y < costmap.getSizeY() and costmap.getCost(x, y) == (OccupancyGrid2d.CostValues.FreeSpace.value or OccupancyGrid2d.CostValues.NoInformation.value)):
                neighbors.append(fCache.getPoint(x, y))

    return neighbors


def isFrontierPoint(point, costmap, fCache):
    # If a cell is known, then it's not a frontier
    if costmap.getCost(point.mapX, point.mapY) != OccupancyGrid2d.CostValues.NoInformation.value:
        return False

    # This block completed only if the cell is unknown
    hasFree = False
    for n in getNeighbors(point, costmap, fCache):
        cost = costmap.getCost(n.mapX, n.mapY)

        # It means that is cannot be free. Neighbours should be free
        # If at least one is occupied, stop searching and the cell is not a frontier
        if cost > OCC_THRESHOLD:
            return False

        # If one of neighbours is free, then consider a given cell as a frontier
        # At the same time, a cell is not a frontier, if its neighbours are all unknown
        if cost == OccupancyGrid2d.CostValues.FreeSpace.value:
            hasFree = True

    return hasFree

class PointClassification(Enum):
    MapOpen = 1
    MapClosed = 2
    FrontierOpen = 4
    FrontierClosed = 8

class FrontierDiscoverer(Node):

    def __init__(self):
        super().__init__(node_name='nav2_frontier_discoverer', namespace='')
        self.waypoints = None
        self.readyToMove = True
        self.currentPose = None
        self.lastWaypoint = None
        self.action_client = ActionClient(self, FollowWaypoints, 'FollowWaypoints')
        self.twist_pub = self.create_publisher(Twist, 'cmd_vel', 10)
        self.initial_pose_pub = self.create_publisher(PoseWithCovarianceStamped,
                                                      'initialpose', 10)
        self.navigator = BasicNavigator()

        self.costmapClient = self.create_client(GetCostmap, '/global_costmap/get_costmap')
        while not self.costmapClient.wait_for_service(timeout_sec=1.0):
            self.info_msg('service not available, waiting again...')
        self.initial_pose_received = False
        self.goal_handle = None

        pose_qos = QoSProfile(
          durability=QoSDurabilityPolicy.RMW_QOS_POLICY_DURABILITY_TRANSIENT_LOCAL,
          reliability=QoSReliabilityPolicy.RMW_QOS_POLICY_RELIABILITY_RELIABLE,
          history=QoSHistoryPolicy.RMW_QOS_POLICY_HISTORY_KEEP_LAST,
          depth=1)

        self.model_pose_sub = self.create_subscription(Odometry,
                                                       '/odom', self.poseCallback, pose_qos)

        # self.costmapSub = self.create_subscription(Costmap(), '/global_costmap/costmap_raw', self.costmapCallback, pose_qos)
        self.costmapSub = self.create_subscription(OccupancyGrid(), '/map', self.occupancyGridCallback, pose_qos)
        self.costmap = None

        self.get_logger().info('Running Frontier Discoverer')

    # just take the ogm from msg
    def occupancyGridCallback(self, msg):
        self.costmap = OccupancyGrid2d(msg)

    
    def rotate(self):

        msg = Twist()
        msg.linear.x = 0.0
        msg.linear.y = 0.0
        msg.linear.z = 0.0
        msg.angular.x = 0.0
        msg.angular.y = 0.0
        msg.angular.z = 0.5

        current_time = self.get_clock().now()
        last_time = current_time
        # print(f'Time 0: {current_time.seconds_nanoseconds()[0]}')
        # print(f'Time 1: {current_time.seconds_nanoseconds()[1]}')
        self.info_msg("Rotating on-place to explore the map")
        rate = self.create_rate(10)
        # while current_time.seconds_nanoseconds()[0] - last_time.seconds_nanoseconds()[0] < 5.0:

        self.twist_pub.publish(msg)
        # print(f'Diff: {current_time.seconds_nanoseconds()[0] - last_time.seconds_nanoseconds()[0]}')
        # current_time = self.get_clock().now()
        time.sleep(3)


    def moveToFrontiers(self):

        self.info_msg('Moving to frontiers')
        frontiers = getFrontier(self.currentPose, self.costmap, self.get_logger())

        if len(frontiers) == 0:
            self.info_msg('No More Frontiers')
            return
        ################################################################
        ################################################################
        ################################################################
        location = None
        largestDist = 0
        for f in frontiers:
            print(f'Frontier coordinates: {f}')
            dist = math.sqrt(((f[0] - self.currentPose.position.x)**2) + ((f[1] - self.currentPose.position.y)**2))
            if dist > largestDist:
                largestDist = dist
                location = [f] 

        #worldFrontiers = [self.costmap.mapToWorld(f[0], f[1]) for f in frontiers]
        self.info_msg(f'World points: {location}')
        # TODO:
        self.setWaypoints(location)

        nav_start = self.navigator.get_clock().now()
        self.info_msg('Start following waypoints')
        self.navigator.followWaypoints(self.waypoints)

        # action_request = FollowWaypoints.Goal()
        # action_request.poses = self.waypoints

        # self.info_msg('Sending goal request...')
        # # TODO: find out
        # send_goal_future = self.action_client.send_goal_async(action_request)
        # self.info_msg('Goal request sent...')
        # try:
        #     rclpy.spin_until_future_complete(self, send_goal_future)
        #     self.goal_handle = send_goal_future.result()
        # except Exception as e:
        #     self.error_msg('Service call failed %r' % (e,))

        # if not self.goal_handle.accepted:
        #     self.error_msg('Goal rejected')
        #     return

        # self.info_msg('Goal accepted')

        result = self.navigator.getResult()
        # get_result_future = self.goal_handle.get_result_async()

        # TODO: Wait here for the goal to be reached?
        i = 0
        while not self.navigator.isTaskComplete():

            i = i + 1
            feedback = self.navigator.getFeedback()
            if feedback and i % 5 == 0:
                print('Executing current waypoint: ' +
                    str(feedback.current_waypoint + 1) + '/' + str(len(self.waypoints)))
            now = self.navigator.get_clock().now()
 
            # Some navigation timeout to demo cancellation
            if now - nav_start > Duration(seconds=100000000.0):
                self.navigator.cancelNav()

        result = self.navigator.getResult()
        if result == TaskResult.SUCCEEDED:
          print('Goal succeeded!')
          self.rotate()
        elif result == TaskResult.CANCELED:
          print('Goal was canceled!')
        elif result == TaskResult.FAILED:
          print('Goal failed!')
        else:
          print('Goal has an invalid return status!')

        # self.info_msg("Waiting for the goal to be reached")
        # try:
            # rclpy.spin_until_future_complete(self, result)
            # status = result.result().status
            # result = result.result().result
        # except Exception as e:
            # self.error_msg('Service call failed %r' % (e,))

        self.currentPose = self.waypoints[len(self.waypoints) - 1].pose
        print(f'Current pose: {self.currentPose}')

        self.moveToFrontiers()

    def costmapCallback(self, msg):
        self.costmap = Costmap2d(msg)

        unknowns = 0
        for x in range(0, self.costmap.getSizeX()):
            for y in range(0, self.costmap.getSizeY()):
                if self.costmap.getCost(x, y) == 255:
                    unknowns = unknowns + 1
        self.get_logger().info(f'Unknowns {unknowns}')
        self.get_logger().info(f'Got Costmap {len(getFrontier(None, self.costmap, self.get_logger()))}')

    def dumpCostmap(self):
        costmapReq = GetCostmap.Request()
        self.get_logger().info('Requesting Costmap')
        costmap = self.costmapClient.call(costmapReq)
        self.get_logger().info(f'costmap resolution {costmap.specs.resolution}')

    def setInitialPose(self, pose):
        self.init_pose = PoseWithCovarianceStamped()
        self.init_pose.pose.pose.position.x = pose[0]
        self.init_pose.pose.pose.position.y = pose[1]
        self.init_pose.header.frame_id = 'map'
        self.currentPose = self.init_pose.pose.pose
        # self.publishInitialPose()
        time.sleep(1)

    def poseCallback(self, msg):
        self.info_msg('Received pose')
        self.currentPose = msg.pose.pose
        self.initial_pose_received = True
        

    def setWaypoints(self, waypoints):
        self.waypoints = []
        for wp in waypoints:
            msg = PoseStamped()
            msg.header.frame_id = 'map'
            msg.header.stamp = self.navigator.get_clock().now().to_msg()
            msg.pose.position.x = wp[0]
            msg.pose.position.y = wp[1]
            msg.pose.orientation.w = 1.0
            self.waypoints.append(msg)

    def run(self, block):
        if not self.waypoints:
            rclpy.error_msg('Did not set valid waypoints before running test!')
            return False

        while not self.action_client.wait_for_server(timeout_sec=1.0):
            self.info_msg("'FollowWaypoints' action server not available, waiting...")

        action_request = FollowWaypoints.Goal()
        action_request.poses = self.waypoints

        self.info_msg('Sending goal request...')
        send_goal_future = self.action_client.send_goal_async(action_request)
        # self.info_msg('Goal request sent...')
        try:
            rclpy.spin_until_future_complete(self, send_goal_future)
            self.goal_handle = send_goal_future.result()
        except Exception as e:
            self.error_msg('Service call failed %r' % (e,))

        if not self.goal_handle.accepted:
            self.error_msg('Goal rejected')
            return False

        self.info_msg('Goal accepted')
        if not block:
            return True

        get_result_future = self.goal_handle.get_result_async()

        self.info_msg("Waiting for 'FollowWaypoints' action to complete")
        try:
            rclpy.spin_until_future_complete(self, get_result_future)
            status = get_result_future.result().status
            result = get_result_future.result().result
        except Exception as e:
            self.error_msg('Service call failed %r' % (e,))

        if status != GoalStatus.STATUS_SUCCEEDED:
            self.info_msg('Goal failed with status code: {0}'.format(status))
            return False
        if len(result.missed_waypoints) > 0:
            self.info_msg('Goal failed to process all waypoints,'
                          ' missed {0} wps.'.format(len(result.missed_waypoints)))
            return False

        self.info_msg('Goal succeeded!')
        return True

    def publishInitialPose(self):
        self.initial_pose_pub.publish(self.init_pose)

    def shutdown(self):
        self.info_msg('Shutting down')

        self.action_client.destroy()
        self.info_msg('Destroyed FollowWaypoints action client')

        transition_service = 'lifecycle_manager_navigation/manage_nodes'
        mgr_client = self.create_client(ManageLifecycleNodes, transition_service)
        while not mgr_client.wait_for_service(timeout_sec=1.0):
            self.info_msg(transition_service + ' service not available, waiting...')

        req = ManageLifecycleNodes.Request()
        req.command = ManageLifecycleNodes.Request().SHUTDOWN
        future = mgr_client.call_async(req)
        try:
            rclpy.spin_until_future_complete(self, future)
            future.result()
        except Exception as e:
            self.error_msg('%s service call failed %r' % (transition_service, e,))

        self.info_msg('{} finished'.format(transition_service))

        transition_service = 'lifecycle_manager_localization/manage_nodes'
        mgr_client = self.create_client(ManageLifecycleNodes, transition_service)
        while not mgr_client.wait_for_service(timeout_sec=1.0):
            self.info_msg(transition_service + ' service not available, waiting...')

        req = ManageLifecycleNodes.Request()
        req.command = ManageLifecycleNodes.Request().SHUTDOWN
        future = mgr_client.call_async(req)
        try:
            rclpy.spin_until_future_complete(self, future)
            future.result()
        except Exception as e:
            self.error_msg('%s service call failed %r' % (transition_service, e,))

        self.info_msg('{} finished'.format(transition_service))

    def cancel_goal(self):
        cancel_future = self.goal_handle.cancel_goal_async()
        rclpy.spin_until_future_complete(self, cancel_future)

    def info_msg(self, msg: str):
        self.get_logger().info(msg)

    def warn_msg(self, msg: str):
        self.get_logger().warn(msg)

    def error_msg(self, msg: str):
        self.get_logger().error(msg)


def main():
    
    rclpy.init()

    frontier_discoverer = FrontierDiscoverer()

    # frontier_discoverer.move_to_frontiers()

    starting_pose = [21.32115109682464, -23.17114582423655]

    retry_count = 0
    retries = 2
    # set current pose (first manually, then from subscription)
    while not frontier_discoverer.initial_pose_received and retry_count <= retries:
        retry_count += 1
        frontier_discoverer.info_msg('Setting initial pose')
        frontier_discoverer.setInitialPose(starting_pose)
        frontier_discoverer.info_msg('Waiting for pose')
        rclpy.spin_once(frontier_discoverer, timeout_sec=1.0)  # wait for poseCallback

    while frontier_discoverer.costmap == None:
        frontier_discoverer.info_msg('Getting initial map')
        rclpy.spin_once(frontier_discoverer, timeout_sec=1.0)

    frontier_discoverer.moveToFrontiers()

    rclpy.spin(frontier_discoverer)

    # goal_poses = []

    # print('Creating a goal')
    # goal_pose = PoseStamped()
    # goal_pose.header.frame_id = 'map'
    # goal_pose.header.stamp = navigator.get_clock().now().to_msg()
    # goal_pose.pose.position.x = 21.9093
    # goal_pose.pose.position.y = -22.5
    # goal_pose.pose.position.z = 0.0
    # goal_pose.pose.orientation.x = 0.0
    # goal_pose.pose.orientation.y = 0.0
    # goal_pose.pose.orientation.z = 0.863065
    # goal_pose.pose.orientation.w = 0.505092
    # goal_poses.append(goal_pose)

    # nav_start = navigator.get_clock().now()
    # print('Start following waypoints')
    # navigator.followWaypoints(goal_poses)
    
    # i = 0
    # while not navigator.isTaskComplete():

    #     i = i + 1
    #     feedback = navigator.getFeedback()
    #     if feedback and i % 5 == 0:
    #         print('Executing current waypoint: ' +
    #             str(feedback.current_waypoint + 1) + '/' + str(len(goal_poses)))
    #     now = navigator.get_clock().now()
 
    #     # Some navigation timeout to demo cancellation
    #     if now - nav_start > Duration(seconds=100000000.0):
    #         navigator.cancelNav()

    # result = navigator.getResult()
    # if result == TaskResult.SUCCEEDED:
    #   print('Goal succeeded!')
    # elif result == TaskResult.CANCELED:
    #   print('Goal was canceled!')
    # elif result == TaskResult.FAILED:
    #   print('Goal failed!')
    # else:
    #   print('Goal has an invalid return status!')
 
    # navigator.lifecycleShutdown()

    # exit(0)


if __name__ == '__main__':
    main()