import sys
import time
import numpy as np
import math

from enum import Enum

from action_msgs.msg import GoalStatus
from geometry_msgs.msg import PoseStamped, PoseWithCovarianceStamped, Twist
from nav2_msgs.srv import GetCostmap
from nav2_msgs.msg import Costmap
from nav_msgs.msg  import OccupancyGrid
from nav_msgs.msg import Odometry
from sensor_msgs.msg import LaserScan

import rclpy
from rclpy.duration import Duration
from rclpy.action import ActionClient
from rclpy.node import Node
import rclpy.qos

from nav2_simple_commander.robot_navigator import BasicNavigator, TaskResult


class RightTo(Node):

    def __init__(self):
        super().__init__(node_name='nav2_right_to', namespace='')

        self.current_scan = LaserScan()
        
        self.is_scan_init = False
        
        self.Kv = 0.5
        self.Ktheta = 1
        
        qos_policy = rclpy.qos.QoSProfile(reliability=rclpy.qos.ReliabilityPolicy.BEST_EFFORT,
                                          history=rclpy.qos.HistoryPolicy.KEEP_LAST,
                                          depth=1)

        self.twist_pub = self.create_publisher(Twist, 'cmd_vel', 10)
        self.laser_scan_sub = self.create_subscription(LaserScan, '/scan', self.scan_cbk, qos_profile=qos_policy)

        self.get_logger().info('Running Right To')


    def scan_cbk(self, msg):

        # self.info_msg('Received laser scan')
        self.current_scan = msg
        self.is_scan_init = True
        self.info_msg(f'Amount of ranges: {len(msg.ranges)}')


    # def check_scan(self, th):

    #     scan_num = 80
    #     if self.current_scan.ranges[scan_num] < th:
    #         return True
    #     else:
    #         return False
    
    def get_range(self):
        
        scan_num = 80
        return self.current_scan.ranges[scan_num]
    
    
    def get_angle(self):
        
        scan_num = 80
        return self.current_scan.angle_min + scan_num * self.current_scan.angle_increment
            
            
    def move(self):
        
        # TODO: 1) control to decrease its range
        #       1.1) set control model
        # if the dist is small, we need to decelerate
        # otherwise, accelerate
        # 
        
        th = 0.9
        # if self.check_scan(th):
            # pass
        # else:
            
        twist = Twist()
        range = self.get_range
        angle = self.get_angle
        twist.linear.x = self.Kv * (range ** 2)
        twist.angular.z = self.Ktheta * math.atan2()



    def info_msg(self, msg: str):

        self.get_logger().info(msg)


    def warn_msg(self, msg: str):
        
        self.get_logger().warn(msg)


    def error_msg(self, msg: str):

        self.get_logger().error(msg)


def main():

    rclpy.init()

    right_to = RightTo()
    
    while not right_to.is_scan_init:
        
        rclpy.spin_once(right_to, timeout_sec=1.0)
        time.sleep(1)

    while rclpy.ok():

        right_to.move()
        rclpy.spin_once(right_to, timeout_sec=1.0)


if __name__ == '__main__':
    main()

