#!/usr/bin/env python3

# Written by Nikolay Dema <ndema2301@gmail.com>, September 2022

import os
from re import S
from tracemalloc import start

from ament_index_python.packages import get_package_share_directory

from launch import LaunchDescription

from launch.actions import ExecuteProcess, IncludeLaunchDescription

from launch.launch_description_sources import PythonLaunchDescriptionSource

from launch_ros.actions import Node

from launch_ros.substitutions import FindPackageShare

from launch.substitutions import LaunchConfiguration


def generate_launch_description():

    pkg_path = "/workspace/src/survey"
    os.environ["GAZEBO_MODEL_PATH"] = pkg_path + "/models"

    world_path = pkg_path + "/worlds/ozyland.world"
    rviz_config_path = os.path.join(pkg_path, 'rviz/config.rviz')
    robot_localization_file_path = os.path.join(pkg_path, 'params', 'ekf.yaml')
    static_map_path = os.path.join(pkg_path, 'map', 'map_1664700377.yaml')
    nav2_dir = FindPackageShare(package='nav2_bringup').find('nav2_bringup') 
    nav2_launch_dir = os.path.join(nav2_dir, 'launch')
    nav2_params_path = os.path.join(pkg_path, 'params', 'nav2_params.yaml')
    nav2_bt_path = FindPackageShare(package='nav2_bt_navigator').find('nav2_bt_navigator')
    # slam_toolbox_dir = FindPackageShare(package='slam_toolbox').find('slam_toolbox')
    # slam_toolbox_launch_dir = os.path.join(slam_toolbox_dir, 'launch')
    # slam_toolbox_params_path = os.path.join(pkg_path, 'params', 'slam_toolbox_params.yaml')

    map_yaml_file = LaunchConfiguration('map')
    params_file = LaunchConfiguration('params_file')
    rviz_config_file = LaunchConfiguration('rviz_config_file')
    slam = LaunchConfiguration('slam')
    use_rviz = LaunchConfiguration('use_rviz')
    world = LaunchConfiguration('world')

    remappings = [('/tf', 'tf'),
                  ('/tf_static', 'tf_static')]

    gazebo_cmd = ExecuteProcess(output = "screen",
                                   cmd = ["gazebo",
                                          "--verbose",
                                          "-s",
                                          "libgazebo_ros_init.so",
                                          world_path])

    # Launch RViz
    start_rviz_cmd = Node(package='rviz2',
                          executable='rviz2',
                          name='rviz2',
                          output='screen',
                          arguments=['-d', rviz_config_path])

    start_robot_localization_cmd = Node(package='robot_localization',
                                        executable='ekf_node',
                                        name='ekf_filter_node',
                                        output='screen',
                                        parameters=[robot_localization_file_path,
                                        {'use_sim_time': True}])

    start_ros2_navigation_cmd = IncludeLaunchDescription(
      PythonLaunchDescriptionSource(os.path.join(nav2_launch_dir, 'bringup_launch.py')),
            launch_arguments = {'slam': True,
                        'map': map_yaml_file,
                        'use_sim_time': True,
                        'params_file': params_file}.items())

    # Launch the ROS 2 Navigation Stack
#     start_ros2_navigation_cmd = IncludeLaunchDescription(
#           PythonLaunchDescriptionSource(os.path.join(nav2_launch_dir, 'navigation_launch.py')),
#                launch_arguments = {'params_file': nav2_params_path}.items())

#     start_slam_toolbox_cmd = IncludeLaunchDescription(
#           PythonLaunchDescriptionSource(os.path.join(slam_toolbox_launch_dir, 'online_async_launch.py')),
#           launch_arguments = {'slam_params_file' : slam_toolbox_params_path}.items())

    tf2_base_link_lidar_cmd = Node(package = "tf2_ros",
                                   executable = "static_transform_publisher",
                                   arguments = ["0", "0", "0.775", "0", "0", "0", "base_link", "lidar"],
                                   output = "screen")

    tf2_base_link_camera_cmd = Node(package = "tf2_ros",
                                     executable = "static_transform_publisher",
                                     arguments = ["0.42", "0", "1.75", "-0.5", "0.5", "-0.5", "0.5", "base_link", "camera"],
                                     output = "screen")


    ld = LaunchDescription() # TODO: Figure out about lifecycle !!!

    ld.add_action(gazebo_cmd)
    ld.add_action(tf2_base_link_camera_cmd)
    ld.add_action(tf2_base_link_lidar_cmd)
    ld.add_action(start_rviz_cmd)
    ld.add_action(start_ros2_navigation_cmd)
    ld.add_action(start_robot_localization_cmd)
#     ld.add_action(start_robot_state_publisher_cmd)
#     ld.add_action(start_slam_toolbox_cmd)

    return ld
