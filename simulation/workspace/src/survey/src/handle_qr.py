import rclpy
from rclpy.node import Node

from sensor_msgs.msg import Image

class ImageSubscriber(Node):

    def __init__(self):

        super().__init__('qr_handler')
        self.subscription = self.create_subscription(Image, '/camera/image_raw', self.listener_callback, 10)
        self.subscription

    def listener_callback(self, msg):

        self.get_logger().info('Got an image!')


def main(args=None):

    rclpy.init(args=args)

    image_subscriber = ImageSubscriber()

    rclpy.spin(image_subscriber)

    image_subscriber.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
