#include <chrono>
#include <functional>
#include <memory>
#include <string>

#include "rclcpp/rclcpp.hpp"
#include "sensor_msgs/msg/image.hpp"

using std::placeholders::_1;

class ImageSubscriber : public rclcpp::Node {
    public:
        ImageSubscriber() : Node("image_subscriber") {
            subscription_ = this->create_subscription<sensor_msgs::msg::Image>("/camera/image_raw", 10, std::bind(&ImageSubscriber::cbk, this, _1));
        }

    private:
        void cbk(const sensor_msgs::msg::Image::SharedPtr msg) const {
            RCLCPP_INFO(this->get_logger(), "Got an image");
        }
        rclcpp::Subscription<sensor_msgs::msg::Image>::SharedPtr subscription_;
};

int main(int argc, char** argv) {
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<ImageSubscriber>());
    rclcpp::shutdown();
    return 0;
}