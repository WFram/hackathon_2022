# Import the necessary libraries
from cv2 import CLAHE
import rclpy  # Python library for ROS 2
from rclpy.node import Node  # Handles the creation of nodes
from sensor_msgs.msg import Image
from std_msgs.msg import Empty
from geometry_msgs.msg import Twist
from cv_bridge import CvBridge
import cv2
import pyzbar.pyzbar as pyzbar
import numpy as np
from enum import Enum


class ImageProcessing(Enum):
        
        clahe = 0
        gray_level_slicing = 1


class QRDetector(Node):
    """
    Create QRDetector class, which is a subclass of the Node class.
    """

    def __init__(self, img_preprocess):

        super().__init__('qr_detector')
        qos_policy = rclpy.qos.QoSProfile(reliability=rclpy.qos.ReliabilityPolicy.BEST_EFFORT,
                                          history=rclpy.qos.HistoryPolicy.KEEP_LAST,
                                          depth=1)
        
        self.image_proc_pub = self.create_publisher(Image, '/camera/image_proc', 100)
        
        self.image_th_pub = self.create_publisher(Image, '/camera/image_th', 10)
        
        self.twist_pub = self.create_publisher(Twist, '/cmd_vel', 10)
        
        self.finish_pub = self.create_publisher(Empty, '/finish_discovering', 10)

        self.subscription_image = self.create_subscription(
            Image,
            '/camera/image_raw',
            self.listener_callback,
            qos_profile=qos_policy)
        
        self.img_preprocess = img_preprocess
        self.min_range = 30
        self.max_range = 230
        self.dic = dict()
        self.current_frame = None
        self.br = CvBridge()
        
    
    def finish_discovering(self):

        twist = Twist()
        twist.linear.x = 0.0
        twist.linear.y = 0.0
        twist.linear.z = 0.0
        twist.angular.x = 0.0
        twist.angular.y = 0.0 
        twist.angular.z = 0.0
        self.twist_pub.publish(twist)
        
        self.save_qr_data()
        
        
    def QRCheck(self, data, polygon):
        
        data = data.split(" ", 1)
        
        if len(data) > 1:
            self.number = data[0]
            self.sentence = data[1]
            if self.number not in self.dic:
                if len(self.number)==2:
                    self.number = '0' + self.number
                self.dic[self.number+' '] = self.sentence

        hull = polygon
        # Number of points in the convex hull
        n = len(hull)

        # Draw the convext hull
        for j in range(0, n):
            cv2.line(self.current_frame, hull[j], hull[(j + 1) % n], (255, 0, 0), 3)
    
            
    def qr_decode(self, image):
        
        qr_objs = pyzbar.decode(image)
        for qr_obj in qr_objs:
            self.value = qr_obj.data.decode()
            self.polygon = qr_obj.polygon
            self.QRCheck(self.value, self.polygon)
        
        self.get_logger().info(str(dict(sorted(self.dic.items()))))
        
    
    def slice_gray_level(self, image):
        
        LUT = []
        
        for i in range(256):
            if i > self.min_range and i < self.max_range:
                LUT.append(255)
            else:
                LUT.append(i)
            
        LUT = np.array(LUT, dtype=np.uint8)
        self.sliced = LUT[image]
    
    
    def listener_callback(self, data):
        
        """
        Callback function
        """
        
        self.current_frame = self.br.imgmsg_to_cv2(data)
        gray = cv2.cvtColor(self.current_frame, cv2.COLOR_BGR2GRAY)
        
        if self.img_preprocess == ImageProcessing.clahe:
            
            clahe = cv2.createCLAHE(clipLimit=4.0, tileGridSize=(8, 8))
            self.equalized = clahe.apply(gray)
            self.qr_decode(self.equalized)
            img_proc_msg = self.br.cv2_to_imgmsg(self.current_frame, "bgr8")
            img_proc_msg.header.stamp = self.get_clock().now().to_msg()
            self.image_proc_pub.publish(img_proc_msg)
            # img_clahe_msg = self.br.cv2_to_imgmsg(self.equalized)
            # img_clahe_msg.header.stamp = self.get_clock().now().to_msg()
            # self.image_th_pub.publish(img_clahe_msg)
            
        elif self.img_preprocess == ImageProcessing.gray_level_slicing:
            
            rows, cols = gray.shape
            self.sliced = np.zeros((rows, cols), dtype = 'uint8')
            self.slice_gray_level(gray)
            self.qr_decode(self.sliced)
            img_proc_msg = self.br.cv2_to_imgmsg(self.current_frame, "bgr8")
            img_proc_msg.header.stamp = self.get_clock().now().to_msg()
            self.image_proc_pub.publish(img_proc_msg)
            # img_th_msg = self.br.cv2_to_imgmsg(self.sliced)
            # img_th_msg.header.stamp = self.get_clock().now().to_msg()
            # self.image_th_pub.publish(img_th_msg)
            
        if len(self.dic) == 15:
            
            self.finish_discovering()
            
        
    def save_qr_data(self):
        
        print("All QR codes detected! Shutdown")
        
        story = dict(sorted(self.dic.items()))
        with open("/workspace/mystory.txt", 'w') as f:
            for key, value in story.items():
                f.write('%s:%s\n' % (key, value))

        # Shutdown the ROS client library for Python
        empty = Empty()
        self.finish_pub.publish(empty)
        
        rclpy.shutdown()


def main(args=None):
    # Initialize the rclpy library
    rclpy.init(args=args)

    # Create the node
    qr_detector = QRDetector(ImageProcessing.gray_level_slicing)

    # Spin the node so the callback function is called.
    rclpy.spin(qr_detector)
    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    qr_detector.destroy_node()


if __name__ == '__main__':
    main()
