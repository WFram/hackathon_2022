# Import the necessary libraries
import rclpy  # Python library for ROS 2
from rclpy.node import Node  # Handles the creation of nodes
from sensor_msgs.msg import Image
from std_msgs.msg import Empty
from cv_bridge import CvBridge
import cv2
import pyzbar.pyzbar as pyzbar

class ImageSubscriber(Node):
    """
    Create an ImageSubscriber class, which is a subclass of the Node class.
    """

    def __init__(self):

        super().__init__('image_subscriber')
        qos_policy = rclpy.qos.QoSProfile(reliability=rclpy.qos.ReliabilityPolicy.BEST_EFFORT,
                                          history=rclpy.qos.HistoryPolicy.KEEP_LAST,
                                          depth=1)

        self.subscription_image = self.create_subscription(
            Image,
            '/camera/image_raw',
            self.listener_callback,
            qos_profile=qos_policy)

        self.subscription_finish = self.create_subscription(
            Empty,
            '/finish_detect',
            self.end_callback,10)
        self.subscription_image
        self.subscription_finish
        self.dic = dict()
        self.current_frame = None
        self.br = CvBridge()
    def QRCheck(self, data,polygon):
        data = data.split(" ", 1)
        if len(data)>1:
            self.number = data[0]
            self.sentence = data[1]
            if self.number not in self.dic:
                if len(self.number)==2:
                    self.number = '0' + self.number
                self.dic[self.number+' '] = self.sentence

        hull = polygon
        # Number of points in the convex hull
        n = len(hull)

        # Draw the convext hull
        for j in range(0, n):
            cv2.line(self.equalized, hull[j], hull[(j + 1) % n], (255, 0, 0), 3)
    def listener_callback(self, data):
        """
        Callback function.
        """
        self.current_frame = self.br.imgmsg_to_cv2(data)
        gray = cv2.cvtColor(self.current_frame, cv2.COLOR_BGR2GRAY)
        #equalized = cv2.equalizeHist(gray)
        clahe = cv2.createCLAHE(clipLimit=4.0, tileGridSize=(8, 8))
        self.equalized = clahe.apply(gray)
        qr_objs = pyzbar.decode(self.equalized)
        for qr_obj in qr_objs:
            self.value = qr_obj.data.decode()
            self.polygon = qr_obj.polygon
            self.QRCheck(self.value,self.polygon)

        self.get_logger().info(str(dict(sorted(self.dic.items()))))

        cv2.imshow("current view", self.equalized)

        cv2.waitKey(1)
    def end_callback(self,msg):
        story = dict(sorted(self.dic.items()))
        with open("/workspace/mystory.txt", 'w') as f:
            for key, value in story.items():
                f.write('%s:%s\n' % (key, value))

        # Shutdown the ROS client library for Python
        rclpy.shutdown()




def main(args=None):
    # Initialize the rclpy library
    rclpy.init(args=args)

    # Create the node
    image_subscriber = ImageSubscriber()

    # Spin the node so the callback function is called.
    rclpy.spin(image_subscriber)
    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    image_subscriber.destroy_node()


if __name__ == '__main__':
    main()