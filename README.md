<!-- Written by Nikolay Dema <ndema2301@gmail.com>, September 2022 -->

### Хакатон СтарЛайн 2022

Репозиторий содержит ПО, разработанное командой **Be2RLab** для участия в Хакатоне, проводимом
НПО СтарЛайн в 2022 году.

Видео-демонстрация работы предоставляемого решения доступно по [ссылке](https://drive.google.com/file/d/1YBpuUpMvMLiudXatxyW2aNvacQ-f72bR/view?usp=sharing).

### Работа с симулятором

![bereg_in_ozyland](docs/pics/bereg_in_ozyland.gif)

Симуляция включает модель небольшого, затерянного в песках города и робота,
имеющего на борту rgbd-камеру и лидар.

В качестве симулятора используется [gazebo](https://classic.gazebosim.org/).
В качестве основного фреймворка для разработки используется
[ROS2](https://docs.ros.org/en/galactic/).

Работа с симулятором предполагается внутри docker-контейнера, скрипты и
утилиты для работы с которым предоставляются в составе репозитория.

#### Установка требуемого ПО

1. Склонируйте репозиторий и перейдите в директорию с исходными файлами репозитория:

        git clone https://gitlab.com/WFram/hackathon_2022.git
        cd hackathon_2022/simulation/workspace/src

2. Склонируйте официальные репозитории навигационного стека **Nav2** и конвертера карты затрат. После этого перейдите в корневую папку репозитория:

        git clone -b galactic https://github.com/ros-planning/navigation2.git
        git clone -b ros2 https://github.com/rst-tu-dortmund/costmap_converter.git
        cd hackathon_2022

3. Установите docker:

    Для установки можно воспользоваться скриптом ниже. В случае, если на вашем
    ПК используется видеокарта от nvidia, то обозначенный скрипт следует
    исполнить с параметром **-n** или **--nvidia**. В этом случае на хост
    дополнительно будет установлен NVIDIA Container Toolkit

        bash scripts/docker_install.bash

    **Для установки вручную** воспользуйтесь
    [официальной инструкцией установки docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/).
    В случае, если на вашем ПК используется видеокарта от nvidia, то дополнительно
    следует установить [nvidia container toolkit](https://github.com/NVIDIA/nvidia-docker).

    После этой операции следует перезайти в систему для корректной работы docker.


#### Использование docker-контейнера и организация процесса разработки

1. Для работы с докер-контейнером доступны следующие скрипты:

        simulation/docker/run.bash  - запуск контейнера
        simulation/docker/into.bash - запуск bash-сессии в контейнере
        simulation/docker/stop.bash - остановка контейнера

    Соответственно, для запуска контейнера запустите скрипт:

        bash simulation/docker/run.bash

    _При первом запуске образ для контейнера скачается автоматически._

    Для выхода из bash-сессии используйте сочетание клавиш ```Ctrl + D```.

2. Рабочее окружение colcon расположено в ```simulation/workspace``` и монтируется
внутрь контейнера в корень файловой системы, таким образом разработку можно
вести как внутри, так и вне контейнера.

    Для сборки пакетов откройте новую bash-сессию в контейнере, перейдите в
    директорию окружения:

        bash simulation/docker/into.bash
        cd /workspace

    Установите необходимые зависимости для пакетов репозитория:

        apt update
        apt-get install -y ros-galactic-bondcpp \ 
                           ros-galactic-test-msgs \
                           ros-galactic-behaviortree-cpp-v3 \
                           ros-galactic-ompl \
                           ros-galactic-libg2o \
                           ros-galactic-slam-toolbox \
                           graphicsmagick-libmagick-dev-compat \
                           python3-opencv \
                           libzbar0 \
                           pip
        pip install pyzbar

    Выполните сборку пакетов и загрузите переменные окружения:

        colcon build
        source install/setup.bash

#### Описание предоставляемого ПО

Для запуска симуляции в первом терминале воспользуйтесь launch-файлом из пакета survey:

    ros2 launch survey ozyland_slam.launch.py

После этого в окне gazebo вы должны увидеть примерно следующую картинку:

![turtletown_gazebo](docs/pics/ozyland.png)

Для запуска узла автономной навигации в неизвестном окружении откройте новую bash-сессию в контейнере из второго терминала и воспользуйтесь следующей командой:

    ros2 run frontier_discoverer discoverer

В результате робот начнёт исследование окружения с целью поиска QR-кодов.


#### Docker Image

На случай возникновения непредвиденных ошибок предоставляется [docker-изображение](https://hub.docker.com/repository/docker/andrewfram/galactic_discovering), содержащее все пакеты репозитория и вышеуказанные зависимости.

Для загрузки изображения воспользуйтесь командой:

    docker pull andrewfram/galactic_discovering:final

Создайте docker-контейнер посредством набора команд:

    export GPU_FLAG=(--gpus all)
    xhost +local:docker > /dev/null || true

    sudo docker run ${GPU_FLAG[@]} \
            -d -ti --rm \
            -e "DISPLAY" \
            -e "QT_X11_NO_MITSHM=1" \
            -e XAUTHORITY \
            -v /tmp/.X11-unix:/tmp/.X11-unix:rw \
            -v /etc/localtime:/etc/localtime:ro \
            --net=host \
            --privileged \
            --name "hsl_2022_be2r" andrewfram/galactic_discovering:final \
            > /dev/null

Для дальнейшей работы откройте новую bash-сессию в созданном контейнере:

    sudo docker exec -it "hsl_2022_be2r" /bin/bash